import cv2
import numpy as np
from math import hypot
import keyboard


def order_points_new(pts):
    xSorted = pts[np.argsort(pts[:, 0]), :]
    leftMost = xSorted[:2, :]
    rightMost = xSorted[2:, :]
    leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
    (tl, bl) = leftMost
    rightMost = rightMost[np.argsort(rightMost[:, 1]), :]
    (tr, br) = rightMost
    return np.array([tl, tr, br, bl], dtype="int64")

def search_length(referense=122.65):
    vid = cv2.VideoCapture(0)
    k = 0
    img = None
    px_len_list = []
    koef = 2.4057155372211376
    while True:
        if k < 50:
            k += 1
            ret, frame = vid.read()
        else:
            ret, frame = vid.read()
            img = frame
            # dst = cv2.GaussianBlur(img, (5, 5), cv2.BORDER_DEFAULT)
            img_grey = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            thresh = 87
            ret,thresh_img = cv2.threshold(img_grey, thresh, 255, cv2.THRESH_BINARY)
            contours, hierarchy = cv2.findContours(thresh_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
            cnt = []
            if len( contours) > 0:
                contours = sorted(contours, key=lambda x: cv2.contourArea(x), reverse= True)
                for i in contours:
                    if cv2.contourArea(i) > 10000 and cv2.contourArea(i) < 120000:
                        cnt = i
                        print(k, cv2.contourArea(i))
                        print(k, 'len:', len(i))
                if len(cnt) > 0:
                    # print(k, '.', cnt)
                    print("Length contours: ", len(contours))
                    print("Area cnt:", cv2.contourArea(cnt))
                    rect = cv2.minAreaRect(cnt)
                    box = cv2.boxPoints(rect)
                    box = np.int0(box)
                    box = order_points_new(box)

                    px_len = hypot(box[2][0] - box[1][0], box[2][1] - box[1][1])
                    mm_len = px_len / koef

                    if keyboard.is_pressed('p'):
                        koef = px_len / referense
                    print('koef = ', koef)
                    print('dist(px) =', px_len)
                    print('dist(mm) =', mm_len)
                    img_contours = np.zeros(img.shape)
                    cv2.polylines(img_contours, [box], True, (0,0,255), 2)
                    cv2.line(img, box[2], box[1], (0,0,255), 2)
                    cv2.putText(img, str(int(mm_len))+' mm.', box[1], cv2.FONT_HERSHEY_SIMPLEX, 1, (209, 80, 0, 255), 2)
                    cv2.drawContours(img_contours, cnt, -1, (0,255,0), 1)
                    return int(mm_len)
                else:
                    print('Cannot find contour')
                    return None
            break
