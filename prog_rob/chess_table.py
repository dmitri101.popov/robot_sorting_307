import json

import cv2
import imutils
import numpy as np
import time
import threading
from playsound import playsound

class Table():
    import cv2
    import imutils
    import numpy as np
    import math
    import os
    import torch
    import pandas
    import pathlib

    def play_sound_robot_swap_pawn_to_queen(self):
        time.sleep(5)
        playsound('src/volumes/chess_robot_pawn_queen.mp3')

    def play_sound_robot_swap_pawn_to_rook(self):
        time.sleep(5)
        playsound('src/volumes/chess_robot_pawn_rook.mp3')

    def play_sound_robot_swap_pawn_to_bishop(self):
        time.sleep(5)
        playsound('src/volumes/chess_robot_pawn_bishop.mp3')

    def play_sound_robot_swap_pawn_to_knight(self):
        time.sleep(5)
        playsound('src/volumes/chess_robot_pawn_knight.mp3')

    def play_sound_player_swap_pawn_to_queen(self):
        playsound('src/volumes/chess_player_pawn_ascend.mp3')
        
    def play_sound_wrong_move(self):
        playsound('src/volumes/chess_player_wrong_move.mp3')

    def play_sound_no_move(self):
        playsound('src/volumes/chess_player_no_move.mp3')

    def play_sound_player_won(self):
        playsound('src/volumes/chess_robot_chekmate.mp3')

    def __init__(self):

        self.CHAR = {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g', 8: 'h'}
        self.INTEG = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 6, 'h': 7}
        self.CENT_CHIPS = {}


    def find_homograhy_matrix(self, img1, img2):
        #cells = []

        def find_contours(img):
            _, img = cv2.threshold(img, 100, 200, cv2.THRESH_BINARY_INV)
            contours, hierarchy = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            ret_box = []
            for cnt in contours:
                rect = cv2.minAreaRect(cnt)  # пытаемся вписать прямоугольник
                box = cv2.boxPoints(rect)  # поиск четырех вершин прямоугольника
                box = np.int0(box)  # округление координат
                center = (int(rect[0][0]), int(rect[0][1]))
                area = int(rect[1][0] * rect[1][1])
                if area > 500_000 and area < 1_500_000:
                    xsr = (box[0][0] + box[2][0]) // 2
                    ysr = (box[0][1] + box[2][1]) // 2
                    box = order_points_new(box)

                    if ((box[1][0] - box[0][0]) / (box[3][1] - box[0][1]) < 1.4) and ((box[3][1] - box[0][1]) / (box[1][0] - box[0][0]) < 1.4):
                        if (ret_box == []):
                            ret_box = box
                        elif (abs(box[2][0] - box[0][0]) * abs(box[2][1] - box[0][1])) > \
                                (abs(ret_box[2][0] - ret_box[0][0]) * abs(ret_box[2][1] - ret_box[0][1])):
                            ret_box = box
            try:
                img = cv2.drawContours(img, [ret_box], 0, (0, 255, 255), 5)
                print(abs(ret_box[2][0] - ret_box[0][0]) * abs(ret_box[2][1] - ret_box[0][1]),
                      (ret_box[1][0] - ret_box[0][0]) / (ret_box[3][1] - ret_box[0][1]),
                      (ret_box[3][1] - ret_box[0][1]) / (ret_box[1][0] - ret_box[0][0]))
                cv2.imwrite('1.jpg', img)
                return ret_box
            except:
                print('Контуры не найдены')
                return

        def order_points_new(pts):
            xSorted = pts[np.argsort(pts[:, 0]), :]
            leftMost = xSorted[:2, :]
            rightMost = xSorted[2:, :]
            leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
            (tl, bl) = leftMost
            rightMost = rightMost[np.argsort(rightMost[:, 1]), :]
            (tr, br) = rightMost
            return np.array([tl, tr, br, bl], dtype="int64")


        box1 = find_contours(img2) # эталонная доска
        if (box1 == []):
            return None
        box1 = order_points_new(box1)
        # print(box1)
        box2 = find_contours(img1) # текущая доска с фигурами
        if (box2 == []):
            return None
        box2 = order_points_new(box2)
        # print(box2)

        h, status = cv2.findHomography(box2, box1)

        # start = np.array([box2[0], box2[1], 1])

        # print(start)
        # print(np.dot(h, start))
        board_center_x = (box2[2][0] + box2[0][0]) // 2
        board_center_y = (box2[2][1] + box2[0][1]) // 2
        return h, board_center_x, board_center_y

    def find_absolute_coord(self, x: float, y: float, h: np.array):
        new_vector = np.array([x, y, 1])

        abs_coord = np.dot(h, new_vector)

        x = abs_coord[0]/abs_coord[2]
        y = abs_coord[1]/abs_coord[2]
        return x, y


    def chips_on_NN(self, img, model, BOARD, KOORD):

        import cv2
        import numpy as np

        # cv2.imwrite('test_cam.jpg', img)

        INTEG = self.INTEG

        BOARD_copy = BOARD.copy()
        BOARD_copy[:][:] = 0

        # image2 = img
        # alpha = 1
        # beta = 2
        # image = np.uint8(np.clip((alpha * image2 + beta), 0, 255))

        img = np.uint8(img)
        image = img
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


        start_table = cv2.imread('table/table.bmp')
        start_table = imutils.rotate_bound(start_table, angle=90)
        start_table = np.uint8(start_table)
        start_table = cv2.cvtColor(start_table, cv2.COLOR_BGR2GRAY)

        homo_matrix, board_center_x, board_center_y = self.find_homograhy_matrix(img, start_table)
        if (homo_matrix is None):
            return None, None
        results = model(image)
        results.save()

        import base64
        is_success, im_buf_arr = cv2.imencode(".jpg", image)
        base64_im = base64.b64encode(im_buf_arr)

        pandas_table = results.pandas().xyxy[0]
        pandas_table['center_cell_x'] = (pandas_table.xmax + pandas_table.xmin) / 2
        pandas_table['center_cell_y'] = (pandas_table.ymax + pandas_table.ymin) / 2
        print(len(pandas_table['name'].values))
        print(pandas_table)

        for x, y, h, w, name in zip(pandas_table['center_cell_x'].values, pandas_table['center_cell_y'].values,
                                    (pandas_table.xmax - pandas_table.xmin) / 2,
                                    (pandas_table.ymax - pandas_table.ymin) / 2, pandas_table['name'].values):


            if name != '-desk':
                # корректировка центров фигур из-за перспективы изображения
                if (h > w) and (x > board_center_x):
                    x -= (h - w) / 2
                    h = w
                elif (h > w) and (x < board_center_x):
                    x += (h - w) / 2
                    h = w

                if (w > h) and (y > board_center_y):
                    y -= (w - h) / 2
                    w = h
                elif (w > h) and (y < board_center_y):
                    y += (w - h) / 2
                    w = h

                x1, y1 = self.find_absolute_coord(x, y, homo_matrix)

                for k, v in KOORD.items():
                    cell = '%s' % (k)
                    temp = v
                    if float(x1) > temp[4] - (abs(temp[0]-temp[1])) // 2\
                            and float(y1) > temp[5] - abs(temp[2]-temp[3]) // 2\
                            and float(x1) < temp[4] + abs(temp[0]-temp[1]) // 2\
                            and float(y1) < temp[5] + abs(temp[2]-temp[3]) // 2:
                        image = cv2.rectangle(image, (int(x - h), int(y - w)), (int(x + h), int(y + w)), (0, 0, 255), 5)
                        image = cv2.circle(image, (int(x1), int(y1)), 3, (255, 255, 0), 10)
                        cv2.putText(image, '%s' % (str(k)), (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 3,
                                    cv2.LINE_AA)
                        if str(name)[:5] == 'white':
                            BOARD_copy[INTEG[cell[:1]]][int(cell[1:]) - 1] = 1
                            pandas_table
                            break
                        else:
                            BOARD_copy[INTEG[cell[:1]]][int(cell[1:]) - 1] = -1
                            break


        cv2.imwrite('src/img/test.jpg', image)

        return BOARD_copy, pandas_table, base64_im

    def get_steps(self, board, New_board, TURN, white_black, stockfish):

        import numpy as np

        temp = []
        INTEG = self.INTEG
        for i in TURN:
            temp.append(i[:len(i) - 1])
        stockfish.set_position(temp)
        char = {7: 'a', 6: 'b', 5: 'c', 4: 'd', 3: 'e', 2: 'f', 1: 'g', 0: 'h'}
        if white_black == 'black':
            promotion = ''
            move = stockfish.get_best_move()
            if move is None:
                checkmate = True
                threading.Thread(target=self.play_sound_player_won).start()
                print('ШАХ ИЛИ МАТ')
                return checkmate
            p_1_str = move[:2]
            p_2_str = move[2:4]
            p_1 = INTEG[move[:1]], int(move[1:2]) - 1
            p_2 = INTEG[move[2:3]], int(move[3:4]) - 1
            if (len(move) == 5):
                promotion = move[4:]
            if (promotion == 'q'):
                threading.Thread(target=self.play_sound_robot_swap_pawn_to_queen).start()
                print('Заменить пешку черных на королеву!')
            elif (promotion == 'r'):
                threading.Thread(target=self.play_sound_robot_swap_pawn_to_rook).start()
                print('Заменить пешку черных на ладью!')
            elif (promotion == 'b'):
                threading.Thread(target=self.play_sound_robot_swap_pawn_to_bishop).start()
                print('Заменить пешку черных на слона!')
            elif (promotion == 'n'):
                threading.Thread(target=self.play_sound_robot_swap_pawn_to_knight).start()
                print('Заменить пешку черных на коня!')
            turn_str = '%s%s%s' % (p_1_str, p_2_str, promotion)
            turn = p_1, p_2

            return turn, turn_str
        else:
            turn_str = None
            p_1_str = ''
            p_2_str = ''
            p_3_str = ''
            turn = []
            temp = []
            for i in TURN:
                temp.append(i[:len(i) - 1])
            stockfish.set_position(temp)
            move_matrix = New_board - board
            move = np.argwhere(move_matrix)
            rock_little = np.array([[0, 7], [1, 7], [2, 7], [3, 7]])
            rock_long = np.array([[3, 7], [4, 7], [5, 7], [7, 7]])

            if np.array_equal(move, rock_long): #  move_martix при длинной рокировке
                turn_str = 'e1c1'
                if stockfish.is_move_correct(turn_str):
                    turn = ((4, 7), (2, 7))  # правильный ход
                    print('Длинная рокировка')
                else:
                    print("Неверный ход: ", turn_str)
                    turn_str = ''
                    threading.Thread(target=self.play_sound_wrong_move).start()
                return turn, turn_str

            if np.array_equal(move, rock_little): #  move_martix при короткой рокировке
                turn_str = 'e1g1'
                if stockfish.is_move_correct(turn_str):
                    turn = ((4, 7), (6, 7))  # правильный ход
                    print('Короткая рокировка')
                else:
                    print("Неверный ход: ", turn_str)
                    turn_str = ''
                    threading.Thread(target=self.play_sound_wrong_move).start()
                return turn, turn_str

            if len(move) == 4: # Кроме рокировок изменения 4 клеток быть не может
                print('Проблемы с распознаванием или не хватает фигур на доске (4 шт.)')
                for i in range(len(move)):
                    p_1_str = '%s%s' % (char[move[i][0]], 8 - move[i][1])
                    print('Проблемы в клетке: ', p_1_str)
                return turn, turn_str # Защита от неправильного распознавания хода пользователя

            if len(move) == 3: # взятие на проходе
                for i in range(len(move)):
                    if (move_matrix[move[i][0], move[i][1]] == -1): # находим клетку из которой начала движение пешка
                        if (p_1_str == ''):
                            p_1 = move[i][0], 7 - move[i][1]
                            p_1_str = '%s%s' % (char[move[i][0]], 8 - move[i][1])
                        else: # при вовторном попадании в условие - косяк распознавания
                            p_1_str = ''
                    if (New_board[move[i][0], move[i][1]] == 1): # находим клетку, в которую переместилась пешка
                        if (p_2_str == ''):
                            p_2 = move[i][0], 7 - move[i][1]
                            p_2_str = '%s%s' % (char[move[i][0]], 8 - move[i][1])
                        else: # при вовторном попадании в условие - косяк распознавания
                            p_2_str = ''
                    if (board[move[i][0], move[i][1]] == -1): # находим клетку, в которой съели фигуру соперника
                        p_3_str = '%s%s' % (char[move[i][0]], 8 - move[i][1])

                if (p_1_str != '') and (p_2_str != ''): # защита от косяков распознавания
                    turn_str = '%s%s' % (p_1_str, p_2_str)
                if turn_str != None:  # только если мы смогли разобрать перемещение фигуры
                    if stockfish.is_move_correct(turn_str):
                        turn = p_1, p_2
                        if (p_3_str != '') and (p_3_str[1:2] == '5') and (p_1_str[1:2] == '5') and (p_2_str[1:2] == '6'):
                            print('Взятие на проходе: ', turn_str, ' Съедена фигура в: ', p_3_str)
                        else:
                            # оставляем ход из 2 клеток и ищем проблемную клетку, чтобы уведомить
                            for i in range(len(move)):
                                if p_1 != (move[i][0], 7 - move[i][1]) and p_2 != (move[i][0], 7 - move[i][1]):
                                    p_3_str = '%s%s' % (char[move[i][0]], 8 - move[i][1])
                            print('Распознан обычный ход из логики взятия на проходе: ', turn_str, ' Ошибка в клетке: ', p_3_str)
                    else:
                        if (p_3_str != ''):
                            turn_str = ''
                            print('Неверный ход (взятие на проходе): ', turn_str, ' Третья клетка: ', p_3_str)
                            threading.Thread(target=self.play_sound_wrong_move).start()
                        else:
                            # чтобы не орать о неверном ходе лишний раз, но при этом тих мирно вывести уведомление о проблемных клетках в консоль
                            for i in range(len(move)):
                                if p_1 != (move[i][0], 7 - move[i][1]) and p_2 != (move[i][0], 7 - move[i][1]):
                                    p_3_str = '%s%s' % (char[move[i][0]], 8 - move[i][1])
                            print('Ошибка распознавания в 3 клетках: ', p_1_str, ', ', p_2_str, ', ', p_3_str)
                else:
                    p_1_str = '%s%s' % (char[move[0][0]], 8 - move[0][1])
                    p_2_str = '%s%s' % (char[move[1][0]], 8 - move[1][1])
                    p_3_str = '%s%s' % (char[move[2][0]], 8 - move[2][1])
                    print('Ошибка распознавания в 3 клетках: ', p_1_str, ', ', p_2_str, ', ', p_3_str)

                return turn, turn_str

            # сюда дойдет только len(move) == 2
            # сложное условие, чтобы исключить больше вариантов, если просто уберут 2 фигуры или некорректно что-то распознаем
            if (move_matrix[move[0][0], move[0][1]] == -1) and \
                    ((move_matrix[move[1][0], move[1][1]] == 1) or (move_matrix[move[1][0], move[1][1]] == 2)):
                p_1 = move[0][0], 7 - move[0][1]
                p_2 = move[1][0], 7 - move[1][1]
                p_1_str = '%s%s' % (char[move[0][0]], 8 - move[0][1])
                p_2_str = '%s%s' % (char[move[1][0]], 8 - move[1][1])
                turn_str = '%s%s' % (p_1_str, p_2_str)
            elif move_matrix[move[1][0], move[1][1]] == -1 and \
                    ((move_matrix[move[0][0], move[0][1]] == 1) or (move_matrix[move[0][0], move[0][1]] == 2)):
                p_1 = move[1][0], 7 - move[1][1]
                p_2 = move[0][0], 7 - move[0][1]
                p_1_str = '%s%s' % (char[move[1][0]], 8 - move[1][1])
                p_2_str = '%s%s' % (char[move[0][0]], 8 - move[0][1])
                turn_str = '%s%s' % (p_1_str, p_2_str)
            else:
                p_1_str = '%s%s' % (char[move[0][0]], 8 - move[0][1])
                p_2_str = '%s%s' % (char[move[1][0]], 8 - move[1][1])
                print('Не удалось распознать ход из 2 клеток: ', p_1_str, ', ', p_2_str)
            if turn_str != None: # только если мы смогли разобрать перемещение фигуры
                if stockfish.is_move_correct(turn_str):
                    turn = p_1, p_2
                elif (p_1[1] == 6) and (p_2[1] == 7) and stockfish.is_move_correct(turn_str + 'q'):
                    # если предыдущее условие вернуло "false" и ввиду отсутствия информации какая именно фигура перемещается
                    # то проверяем вариант, что если фигура перемещается из 7 в 8 ряд доски и предположив,
                    # что передвигаемая человеком фигура - пешка, то ей нужно повышение. Проверяем вариан с повышением до королевы/ферзя
                    # TODO: добавить выбор фигуры пользователем вместо захардкоженного варианта
                    turn_str += 'q' # хардкодим повышение до королевы
                    turn = p_1, p_2
                    threading.Thread(target=self.play_sound_player_swap_pawn_to_queen).start()
                    print('Фигура в ', p_2_str, ' повышена до ферзя')
                else:
                    print('Неверный ход: ', turn_str)
                    turn_str = ''
                    threading.Thread(target=self.play_sound_wrong_move).start()
            return turn, turn_str


    def get_img(self):

        import cv2
        import imutils

        with open('src/ChessConfig.json') as file:
            data = json.load(file)
            file.close()
        cap = cv2.VideoCapture(int(data['VideoCapture']))
        cap.set(3, 1920)
        cap.set(4, 1080)
        cap.set(6, cv2.VideoWriter.fourcc('M', 'J', 'P', 'G'))
        counter = 0
        img = 0
        while (True):
            ret, frame = cap.read()
            if counter == 50:
                img = frame
                break
            counter += 1
        cap.release()
        img = imutils.rotate_bound(img, angle=90)
        import base64
        is_success, im_buf_arr = cv2.imencode(".jpg", img)
        base64_im = base64.b64encode(im_buf_arr)

        return img, base64_im

    def step(self, KOORD, model, stockfish, filename='src/step.txt'):

        import numpy as np
        from client import send_data

        eat = False
        rock_long = False
        rock_lit = False
        INTEG = {'a': 7, 'b': 6, 'c': 5, 'd': 4, 'e': 3, 'f': 2, 'g': 1, 'h': 0}

        BOARD = self.np.array([[-1, -1, 0, 0, 0, 0, 1, 1],
                               [-1, -1, 0, 0, 0, 0, 1, 1],
                               [-1, -1, 0, 0, 0, 0, 1, 1],
                               [-1, -1, 0, 0, 0, 0, 1, 1],
                               [-1, -1, 0, 0, 0, 0, 1, 1],
                               [-1, -1, 0, 0, 0, 0, 1, 1],
                               [-1, -1, 0, 0, 0, 0, 1, 1],
                               [-1, -1, 0, 0, 0, 0, 1, 1]])
        try:
            with open(filename, 'r') as f:
                TURN = f.readline().split(',')
        except:
            with open(filename, 'w') as f:
                TURN = []
        TURN = TURN[:len(TURN) - 1]
        can_e8g8_roque = True
        can_e8c8_roque = True
        can_e1g1_roque = True
        can_e1c1_roque = True
        prev_i = ''
        if len(TURN) > 0:
            temp = 0
            for i in TURN:
                    if i[-1:] == 'b': # examples: e2e4w, e7e8qw (for promotion)
                        if (i[:4] == 'e8g8') and can_e8g8_roque:
                            BOARD[INTEG['e']][0] = 0
                            BOARD[INTEG['g']][0] = -1
                            BOARD[INTEG['h']][0] = 0
                            BOARD[INTEG['f']][0] = -1
                            can_e8g8_roque = False
                            can_e8c8_roque = False
                        elif (i[:4] == 'e8c8') and can_e8c8_roque:
                            BOARD[INTEG['e']][0] = 0
                            BOARD[INTEG['c']][0] = -1
                            BOARD[INTEG['a']][0] = 0
                            BOARD[INTEG['d']][0] = -1
                            can_e8g8_roque = False
                            can_e8c8_roque = False
                        # взятие при проходе
                        elif (prev_i != '') and (prev_i[1:2] == '2')  and (prev_i[3:4] == '4') and \
                                (prev_i[:1] == prev_i[2:3]) and (i[1:2] == '4') and (i[3:4] == '3') and \
                                (i[2:3] == prev_i[2:3]) and (abs(INTEG[str(prev_i[2:3])] - INTEG[str(i[:1])]) == 1):
                            BOARD[INTEG[str(i[:1])]] [8 - int(i[1:2])] = 0
                            BOARD[INTEG[str(i[2:3])]] [8 - int(i[3:4])] = -1
                            BOARD[INTEG[str(prev_i[2:3])]] [8 - int(prev_i[3:4])] = 0
                        else:
                            BOARD[INTEG[str(i[:1])]] [8 - int(i[1:2])] = 0
                            BOARD[INTEG[str(i[2:3])]] [8 - int(i[3:4])] = -1
                            if (i[:2] == 'h8'):
                                can_e8g8_roque = False
                            elif (i[:2] == 'a8'):
                                can_e8c8_roque = False
                            elif (i[:2] == 'e8'):
                                can_e8g8_roque = False
                                can_e8c8_roque = False
                    else:
                        if (i[:4] == 'e1g1') and can_e1g1_roque:
                            BOARD[INTEG['e']][7] = 0
                            BOARD[INTEG['g']][7] = 1
                            BOARD[INTEG['h']][7] = 0
                            BOARD[INTEG['f']][7] = 1
                            can_e1g1_roque = False
                            can_e1c1_roque = False
                        elif (i[:4] == 'e1c1') and can_e1c1_roque:
                            BOARD[INTEG['e']][7] = 0
                            BOARD[INTEG['c']][7] = 1
                            BOARD[INTEG['a']][7] = 0
                            BOARD[INTEG['d']][7] = 1
                            can_e1g1_roque = False
                            can_e1c1_roque = False
                        # взятие при проходе
                        elif (prev_i != '') and (prev_i[1:2] == '7')  and (prev_i[3:4] == '5') and \
                                (prev_i[:1] == prev_i[2:3]) and (i[1:2] == '5') and (i[3:4] == '6') and \
                                (i[2:3] == prev_i[2:3]) and (abs(INTEG[str(prev_i[2:3])] - INTEG[str(i[:1])]) == 1):
                            BOARD[INTEG[str(i[:1])]] [8 - int(i[1:2])] = 0
                            BOARD[INTEG[str(i[2:3])]] [8 - int(i[3:4])] = 1
                            BOARD[INTEG[str(prev_i[2:3])]] [8 - int(prev_i[3:4])] = 0
                        else:
                            BOARD[INTEG[str(i[:1])]] [8 - int(i[1:2])] = 0
                            BOARD[INTEG[str(i[2:3])]] [8 - int(i[3:4])] = 1
                            if (i[:2] == 'h1'):
                                can_e1g1_roque = False
                            elif (i[:2] == 'a1'):
                                can_e1c1_roque = False
                            elif (i[:2] == 'e1'):
                                can_e1g1_roque = False
                                can_e1c1_roque = False
                    prev_i = i
        import json
        json_config = open('src/ChessConfig.json', 'r')
        data = json.load(json_config)
        json_config.close()
        # распознавание с 5 попыток
        attempts = 0
        New_board = None
        nn_results_table = None
        if (data['GameByMyself'] == 'False'):
            while (attempts < 5) and ((New_board is None) or (nn_results_table is None)):
                img_chip, img_bytes = self.get_img()
                # print(base64_img)
                New_board, nn_results_table, base64_img = self.chips_on_NN(img_chip, model, BOARD, KOORD)
                attempts += 1
            if (New_board is None) or (nn_results_table is None):
                print('Проблемы с распознаванием')
                return

            move_matrix = New_board - BOARD
            move = np.argwhere(move_matrix)
            print('New board')
            print(New_board, '\n')
            print('Move Matrix')
            print(move_matrix)
        else:
            move = []
        turn_str = None
        if len(move) == 0:
            # if len(TURN) >= 0:
            if (len(TURN) == 0 or TURN[-1][-1] == 'b'):
                turn_str = ''
                threading.Thread(target=self.play_sound_no_move).start()
                print('Вы не сделали ход!')
            if (data['GameByMyself'] == 'True'): # True - робот играет сам с собой
                turn, turn_str = self.get_steps(BOARD, New_board, TURN, 'black', stockfish)
                if len(TURN) % 2 == 0:
                    turn_str += 'w'
                else:
                    turn_str += 'b'
                TURN.append(turn_str)
                # send_data(TURN, None)
                # send_data(TURN, base64_img)

        elif len(move) >= 2 and len(move) <= 4:
            turn, turn_str = self.get_steps(BOARD, New_board, TURN, 'white', stockfish)
            if (turn_str != None) and (turn_str != ''):
                prev_i = turn_str
                turn_str += 'w'
                TURN.append(turn_str)
                # send_data(TURN, base64_img)
                turn, turn_str = self.get_steps(BOARD, New_board, TURN, 'black', stockfish)
                turn_str += 'b'
                TURN.append(turn_str)
                # send_data(TURN, base64_img)
        else:
            print('Проблемы с распознаванием или не хватает фигур на доске')
            print('Длина хода', len(move))

        if (turn_str != None) and (turn_str != ''):
            print("Ход: ", turn_str)

            if turn_str == 'e8g8b' and can_e8g8_roque:
                rock_lit = True
            elif turn_str == 'e8c8b' and can_e8c8_roque:
                rock_long = True

            if (len(TURN) >= 2):
                tmp = TURN[-2]
                if (len(tmp) > 4):
                    BOARD[INTEG[tmp[:1]]][8 - int(tmp[1:2])] = 0
                    if (tmp[-1:] == 'w'):
                        BOARD[INTEG[tmp[2:3]]][8 - int(tmp[3:4])] = 1
                    elif (tmp[-1:] == 'b'):
                        BOARD[INTEG[tmp[2:3]]][8 - int(tmp[3:4])] = -1

            if (len(TURN) >= 1):
                tmp = TURN[-1]
                if (len(tmp) > 4):
                    BOARD[INTEG[tmp[:1]]][8 - int(tmp[1:2])] = 0
                    if (tmp[-1:] == 'w'):
                        if (BOARD[INTEG[tmp[2:3]]][8 - int(tmp[3:4])] != 0):
                            eat = True
                        BOARD[INTEG[tmp[2:3]]][8 - int(tmp[3:4])] = 1
                    elif (tmp[-1:] == 'b'):
                        if (BOARD[INTEG[tmp[2:3]]][8 - int(tmp[3:4])] != 0):
                            eat = True
                        BOARD[INTEG[tmp[2:3]]][8 - int(tmp[3:4])] = -1

            # взятие на проходе
            if (prev_i != '') and (prev_i[1:2] == '2') and (prev_i[3:4] == '4') and \
                    (prev_i[:1] == prev_i[2:3]) and (turn_str[1:2] == '4') and (turn_str[3:4] == '3') and\
                    (turn_str[2:3] == prev_i[2:3]) and (abs(INTEG[str(prev_i[2:3])] - INTEG[str(turn_str[:1])]) == 1):
                #New_board[INTEG[str(prev_i[2:3])]][8 - int(prev_i[3:4])] = 0 # "съедаем" фигуру белых
                BOARD[INTEG[str(prev_i[2:3])]][8 - int(prev_i[3:4])] = 0  # "съедаем" фигуру белых
                prev_i = prev_i[2:4] #клетка из которой нужно выбросить фигуру в корзину
                eat = True
            else:
                prev_i = ''

            f = open(filename, 'w+')
            for i in TURN:
                f.write('%s,' % (str(i)))
            f.close()

            print("eat=", eat, "rock_long=", rock_long, "rock_long=", rock_lit, "en_passant=", prev_i)

        return turn_str, eat, rock_long, rock_lit, TURN, prev_i


class Table_mini(Table):

    def __init__(self):
        pass

    def step(self, start, finish, white_black, BOARD):

        #CHAR = {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g', 8: 'h'}
        INTEG = {'a': 7, 'b': 6, 'c': 5, 'd': 4, 'e': 3, 'f': 2, 'g': 1, 'h': 0}
        #INTEG = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 6, 'h': 7, }
        #CENT_CHIPS = {}
        eat = False
        rock_long = False
        rock_lit = False
        try:
            if BOARD == None:
                BOARD = self.np.array([[-1, -1, 0, 0, 0, 0, 1, 1],
                                       [-1, -1, 0, 0, 0, 0, 1, 1],
                                       [-1, -1, 0, 0, 0, 0, 1, 1],
                                       [-1, -1, 0, 0, 0, 0, 1, 1],
                                       [-1, -1, 0, 0, 0, 0, 1, 1],
                                       [-1, -1, 0, 0, 0, 0, 1, 1],
                                       [-1, -1, 0, 0, 0, 0, 1, 1],
                                       [-1, -1, 0, 0, 0, 0, 1, 1]])
        except:
            pass

        turn_str = start + finish + white_black
        start = INTEG['%s' % (start[0])], int(start[1]) - 1
        finish = INTEG['%s' % (finish[0])], int(finish[1]) - 1
        if BOARD[finish[0]][finish[1]] == 1 or BOARD[finish[0]][finish[1]] == -1:
            eat = True
        if white_black == 'w':
            BOARD[start[0]][start[1]] = 0
            BOARD[finish[0]][finish[1]] = 1
        elif white_black == 'b':
            BOARD[start[0]][start[1]] = 0
            BOARD[finish[0]][finish[1]] = -1
        if turn_str == 'e1g1w' or turn_str == 'e8g8w' or turn_str == 'e8g8b' or turn_str == 'e1g1b':
            rock_lit = True
        elif turn_str == 'e1c1w' or turn_str == 'e8c8w' or turn_str == 'e1c1b' or turn_str == 'e8c8b':
            rock_long = True
        f = open('src/data.txt', 'w+')
        temp = turn_str[len(turn_str) - 1]
        f.write('%s %s' % (temp[:2], temp[2:4]))
        f.close()
        if eat == True:
            pass
        print(turn_str)
        print(eat,rock_long,rock_lit)

        return eat, rock_long, rock_lit, BOARD

