import torch
import cv2

model = torch.hub.load('ultralytics/yolov5', 'custom', r'../../chess2308.pt')

imgs = ['0 (1).jpg',
        '0 (2).jpg',
        '0 (3).jpg',
        '0 (4).jpg',
        '0 (5).jpg',
        '0 (6).jpg']


for img in imgs:
    image = cv2.imread(img)
    image1 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    results = model(image1)
    results.show()
    results.print()