import cv2
import imutils
import numpy as np
import chess
import chess.engine


def find_homograhy_matrix(self, img1, img2):
    cells = []

    def find_contours(img):
        _, img = cv2.threshold(img, 100, 250, cv2.THRESH_BINARY_INV)
        # kernel = cv2.getStructuringElement(shape=cv2.MORPH_RECT, ksize=(7, 7))
        # img = cv2.morphologyEx(img, cv2.MORPH_GRADIENT, kernel)
        contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_TC89_KCOS)
        # cv2.drawContours(img, contours, -1, (255, 0, 0), 3, cv2.LINE_AA, hierarchy, 1)

        for cnt in contours:
            rect = cv2.minAreaRect(cnt)  # пытаемся вписать прямоугольник
            box = cv2.boxPoints(rect)  # поиск четырех вершин прямоугольника
            box = np.int0(box)  # округление координат
            center = (int(rect[0][0]), int(rect[0][1]))
            area = int(rect[1][0] * rect[1][1])
        contours, hierarchy = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        ret_box = []
        for cnt in contours:
            rect = cv2.minAreaRect(cnt)  # пытаемся вписать прямоугольник
            box = cv2.boxPoints(rect)  # поиск четырех вершин прямоугольника
            box = np.int0(box)  # округление координат
            center = (int(rect[0][0]), int(rect[0][1]))
            area = int(rect[1][0] * rect[1][1])
            if area > 700_000 and area < 1_500_000:
                xsr = (box[0][0] + box[2][0]) // 2
                ysr = (box[0][1] + box[2][1]) // 2
                box = order_points_new(box)
                temp = [box[0][0], box[0][1], box[2][0], box[2][1], xsr, ysr, rect[2]]
                img = cv2.drawContours(img, [box], 0, (0, 255, 255), 5)
                # if (box[1][0] - box[0][0]) * 1.4 >= (box[4][1] - box[0][1]):
                cells.append(temp)
                ret_box = box
                print(area)
        cv2.imwrite('1.jpg', img)
        return ret_box

    def order_points_new(pts):
        xSorted = pts[np.argsort(pts[:, 0]), :]

        leftMost = xSorted[:2, :]
        rightMost = xSorted[2:, :]

        leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
        (tl, bl) = leftMost

        rightMost = rightMost[np.argsort(rightMost[:, 1]), :]
        (tr, br) = rightMost

        return np.array([tl, tr, br, bl], dtype="int64")


stockfish = chess.engine(r'..\stockfish_15_win_x64_avx2\stockfish_15_x64_avx2.exe')

