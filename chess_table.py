import cv2
import imutils
import numpy as np

class Table():
    import cv2
    import imutils
    import numpy as np
    import math
    import os
    import torch
    import pandas
    import pathlib

    def __init__(self):

        self.CHAR = {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g', 8: 'h'}
        self.INTEG = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 6, 'h': 7}
        self.CENT_CHIPS = {}


    def find_homograhy_matrix(self, img1, img2):
        cells = []

        def find_contours(img):
            _, img = cv2.threshold(img, 100, 250, cv2.THRESH_BINARY_INV)
            kernel = cv2.getStructuringElement(shape=cv2.MORPH_RECT, ksize=(7, 7))
            img = cv2.morphologyEx(img, cv2.MORPH_GRADIENT, kernel)
            contours, hierarchy = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

            for cnt in contours:
                rect = cv2.minAreaRect(cnt)  # пытаемся вписать прямоугольник
                box = cv2.boxPoints(rect)  # поиск четырех вершин прямоугольника
                box = np.int0(box)  # округление координат
                center = (int(rect[0][0]), int(rect[0][1]))
                area = int(rect[1][0] * rect[1][1])
            contours, hierarchy = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            ret_box = []
            for cnt in contours:
                rect = cv2.minAreaRect(cnt)  # пытаемся вписать прямоугольник
                box = cv2.boxPoints(rect)  # поиск четырех вершин прямоугольника
                box = np.int0(box)  # округление координат
                center = (int(rect[0][0]), int(rect[0][1]))
                area = int(rect[1][0] * rect[1][1])
                if area > 200_000 and area < 13_000_000:
                    xsr = (box[0][0] + box[2][0]) // 2
                    ysr = (box[0][1] + box[2][1]) // 2
                    temp = [box[0][0], box[0][1], box[2][0], box[2][1], xsr, ysr, rect[2]]
                    # img = cv2.drawContours(img, [box], 0, (0, 255, 255), 5)
                    cells.append(temp)
                    ret_box = box
            return ret_box

        def order_points_new(pts):
            # sort the points based on their x-coordinates
            xSorted = pts[np.argsort(pts[:, 0]), :]

            # grab the left-most and right-most points from the sorted
            # x-roodinate points
            leftMost = xSorted[:2, :]
            rightMost = xSorted[2:, :]

            # now, sort the left-most coordinates according to their
            # y-coordinates so we can grab the top-left and bottom-left
            # points, respectively
            leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
            (tl, bl) = leftMost

            # if use Euclidean distance, it will run in error when the object
            # is trapezoid. So we should use the same simple y-coordinates order method.

            # now, sort the right-most coordinates according to their
            # y-coordinates so we can grab the top-right and bottom-right
            # points, respectively
            rightMost = rightMost[np.argsort(rightMost[:, 1]), :]
            (tr, br) = rightMost

            # return the coordinates in top-left, top-right,
            # bottom-right, and bottom-left order
            return np.array([tl, tr, br, bl], dtype="float32")



        box1 = find_contours(img2)
        box1 = order_points_new(box1)
        print(box1)
        box2 = find_contours(img1)
        box2 = order_points_new(box2)
        print(box2)
        h, status = cv2.findHomography(box2, box1)

        start = np.array([box2[0], box2[1], 1])

        print(start)
        print(np.dot(h, start))

        return h


    def find_absolute_coord(self, x: float, y: float, h: np.array):
        new_vector = np.array([x, y, 1])

        abs_coord = np.dot(h, new_vector)

        x = abs_coord[0]/abs_coord[2]
        y = abs_coord[1]/abs_coord[2]
        return x, y


    def chips_on_NN(self, img, model, BOARD, KOORD):

        import cv2
        import numpy as np

        # cv2.imwrite('test_cam.jpg', img)

        INTEG = self.INTEG

        BOARD_copy = BOARD.copy()
        BOARD_copy[:][:] = 0

        image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = np.uint8(img)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        start_table = cv2.imread('table/table.bmp')
        start_table = imutils.rotate(start_table, angle=-90)
        start_table = np.uint8(start_table)
        start_table = cv2.cvtColor(start_table, cv2.COLOR_BGR2GRAY)

        homo_matrix = self.find_homograhy_matrix(img, start_table)

        results = model(image)
        results.save()

        pandas_table = results.pandas().xyxy[0]
        pandas_table['center_cell_x'] = (pandas_table.xmax + pandas_table.xmin) / 2
        pandas_table['center_cell_y'] = (pandas_table.ymax + pandas_table.ymin) / 2
        for x, y, h, w, name in zip(pandas_table['center_cell_x'].values, pandas_table['center_cell_y'].values,
                                    (pandas_table.xmax - pandas_table.xmin) / 2,
                                    (pandas_table.ymax - pandas_table.ymin) / 2, pandas_table['name'].values):
            x1, y1 = self.find_absolute_coord(x, y, homo_matrix)

            for k, v in KOORD.items():
                cell = '%s' % (k)
                temp = v
                if float(x1) > temp[4] - (abs(temp[0]-temp[1])) // 2\
                        and float(y1) > temp[5] - abs(temp[2]-temp[3]) // 2\
                        and float(x1) < temp[4] + abs(temp[0]-temp[1]) // 2\
                        and float(y1) < temp[5] + abs(temp[2]-temp[3]) // 2:
                    image = cv2.rectangle(image, (int(x - h), int(y - w)), (int(x + h), int(y + w)), (0, 0, 255), 5)
                    image = cv2.circle(image, (int(x1), int(y1)), 3, (255, 255, 0), 10)
                    cv2.putText(image, '%s' % (str(k)), (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 3,
                                cv2.LINE_AA)
                    if str(name)[:5] == 'white':
                        BOARD_copy[INTEG[cell[:1]]][int(cell[1:]) - 1] = 1
                        break
                    else:
                        BOARD_copy[INTEG[cell[:1]]][int(cell[1:]) - 1] = -1
                        break

        cv2.imwrite('src/img/test.jpg', image)

        return BOARD_copy

    def get_steps(self, board, New_board, TURN, white_black, stockfish):

        import numpy as np

        temp = []
        INTEG = self.INTEG
        for i in TURN:
            temp.append(i[:len(i) - 1])
        stockfish.set_position(temp)
        char = {0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e', 5: 'f', 6: 'g', 7: 'h'}
        if white_black == 'white':
            move = stockfish.get_best_move()
            if move == None:
                checkmate = True
                return checkmate
            p_1_str = move[:2]
            p_2_str = move[2:]
            p_1 = INTEG[move[:1]], int(move[1:2]) - 1
            p_2 = INTEG[move[2:3]], int(move[3:4]) - 1
            turn_str = '%s%s' % (p_1_str, p_2_str)
            turn = p_1, p_2

            return turn, turn_str
        else:
            temp = []
            for i in TURN:
                temp.append(i[:len(i) - 1])
            stockfish.set_position(temp)
            move_matrix = New_board - board
            move = np.argwhere(move_matrix)
            rock_little = np.array([[4, 7], [5, 7], [6, 7], [7, 7]])
            rock_long = np.array([[0, 7], [2, 7], [3, 7], [4, 7]])

            if np.array_equal(move, rock_long): #  move_martix при короткой рокировке
                turn = ((4, 7), (2, 7))  # равильный ход
                turn_str = 'e8c8'
                print('Короткая рокировка')
                return turn, turn_str

            if np.array_equal(move, rock_little): #  move_martix при короткой рокировке
                turn = ((4, 7), (6, 7))  # равильный ход
                turn_str = 'e8g8'
                print('Длинная рокировка')
                return turn, turn_str

            for i in range(len(move)):
                for j in range(len(move)):
                    p_1 = move[i][0], move[i][1]
                    p_2 = move[j][0], move[j][1]
                    p_1_str = '%s%s' % (char[move[i][0]], move[i][1] + 1)
                    p_2_str = '%s%s' % (char[move[j][0]], move[j][1] + 1)
                    if stockfish.is_move_correct('%s%s' % (p_1_str, p_2_str)):
                        turn_str = '%s%s' % (p_1_str, p_2_str)
                        turn = p_1, p_2
                        return turn, turn_str

                    elif stockfish.is_move_correct('%s%s' % (p_2_str, p_1_str)):
                        turn_str = '%s%s' % (p_2_str, p_1_str)
                        turn = p_2, p_1
                        return turn, turn_str

    def get_img(self):

        import cv2
        import imutils

        cap = cv2.VideoCapture(0)
        cap.set(3, 1920)
        cap.set(4, 1080)
        cap.set(6, cv2.VideoWriter.fourcc('M', 'J', 'P', 'G'))
        counter = 0
        img = 0
        while (True):
            ret, frame = cap.read()
            if counter == 50:
                img = frame
                break
            counter += 1
        cap.release()
        img = imutils.rotate(img, angle=-90)

        return img

    def step(self, KOORD, model, stockfish, filename='src/step.txt'):

        import numpy as np

        eat = False
        rock_long = False
        rock_lit = False
        INTEG = self.INTEG

        BOARD = self.np.array([[1, 1, 0, 0, 0, 0, -1, -1],
                               [1, 1, 0, 0, 0, 0, -1, -1],
                               [1, 1, 0, 0, 0, 0, -1, -1],
                               [1, 1, 0, 0, 0, 0, -1, -1],
                               [1, 1, 0, 0, 0, 0, -1, -1],
                               [1, 1, 0, 0, 0, 0, -1, -1],
                               [1, 1, 0, 0, 0, 0, -1, -1],
                               [1, 1, 0, 0, 0, 0, -1, -1]])
        try:
            with open(filename, 'r') as f:
                TURN = f.readline().split(',')
        except:
            with open(filename, 'w') as f:
                TURN = []
        TURN = TURN[:len(TURN) - 1]
        if len(TURN) > 0:
            temp = 0
            for i in TURN:
                if i[4:] == 'w':
                    # pass
                    BOARD[INTEG[str(i[:1])]][int(i[1:2]) - 1] = 0
                    BOARD[INTEG[str(i[2:3])]][int(i[3:4]) - 1] = 1
                else:
                    # pass
                    BOARD[INTEG[str(i[:1])]][int(i[1:2]) - 1] = 0
                    BOARD[INTEG[str(i[2:3])]][int(i[3:4]) - 1] = -1
        img_chip = self.get_img()
        New_board = self.chips_on_NN(img_chip, model, BOARD, KOORD)
        move_matrix = New_board - BOARD
        move = np.argwhere(move_matrix)
        print(New_board)
        turn_str = ''
        if len(move) == 0:
            if len(TURN) > 0:
                if TURN[-1][-1] == 'w':
                    print('Вы не сделали ход!')
                    return
            turn, turn_str = self.get_steps(BOARD, New_board, TURN, 'white', stockfish)
            turn_str += 'w'
            TURN.append(turn_str)

        elif len(move) >= 2 and len(move) <= 4:
            turn, turn_str = self.get_steps(BOARD, New_board, TURN, 'black', stockfish)
            turn_str += 'b'
            TURN.append(turn_str)
            turn, turn_str = self.get_steps(BOARD, New_board, TURN, 'white', stockfish)
            turn_str += 'w'
            TURN.append(turn_str)
        # from ARCS import ChessMoveLab
        # ChessMoveLab['text'] = turn_str
        if turn_str == '':
            print('Не удалось сгенерировать ход')
            print("Ход: ", turn_str)
            print('-'*10)
        else:
            print("Ход: ", turn_str)


        if turn_str == 'e1g1w' or turn_str == 'e8g8w':
            rock_lit = True
        elif turn_str == 'e1c1w' or turn_str == 'e8c8w':
            rock_long = True



        if New_board[turn[1][0]][turn[1][1]] == -1:
            eat = True
        k = len(TURN)
        BOARD = New_board
        BOARD[turn[0][0]][turn[0][1]] = 0
        if k % 2 != 0:
            BOARD[turn[1][0]][turn[1][1]] = 1
        else:
            BOARD[turn[1][0]][turn[1][1]] = -1
        f = open('src/data.txt', 'w+')
        temp = TURN[len(TURN) - 1]
        f.write('%s %s' % (temp[:2], temp[2:4]))
        f.close()
        # print(BOARD)
        print(TURN)
        f = open(filename, 'w+')
        for i in TURN:
            f.write('%s,' % (str(i)))
        f.close()

        print("eat =", eat, "rock_long =", rock_long, "rock_long =", rock_lit)

        return eat, rock_long, rock_lit


class Table_mini(Table):

    def __init__(self):
        pass

    def step(self, start, finish, white_black, BOARD):

        CHAR = {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g', 8: 'h'}
        INTEG = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, 'f': 5, 'g': 6, 'h': 7, }
        CENT_CHIPS = {}
        eat = False
        rock_long = False
        rock_lit = False
        try:
            if BOARD == None:
                BOARD = self.np.array([[1, 1, 0, 0, 0, 0, -1, -1],
                                       [1, 1, 0, 0, 0, 0, -1, -1],
                                       [1, 1, 0, 0, 0, 0, -1, -1],
                                       [1, 1, 0, 0, 0, 0, -1, -1],
                                       [1, 1, 0, 0, 0, 0, -1, -1],
                                       [1, 1, 0, 0, 0, 0, -1, -1],
                                       [1, 1, 0, 0, 0, 0, -1, -1],
                                       [1, 1, 0, 0, 0, 0, -1, -1]])
        except:
            pass

        turn_str = start + finish + white_black
        start = INTEG['%s' % (start[0])], int(start[1]) - 1
        finish = INTEG['%s' % (finish[0])], int(finish[1]) - 1
        if BOARD[finish[0]][finish[1]] == 1 or BOARD[finish[0]][finish[1]] == -1:
            eat = True
        if white_black == 'w':
            BOARD[start[0]][start[1]] = 0
            BOARD[finish[0]][finish[1]] = 1
        elif white_black == 'b':
            BOARD[start[0]][start[1]] = 0
            BOARD[finish[0]][finish[1]] = -1
        if turn_str == 'e1g1w' or turn_str == 'e8g8w' or turn_str == 'e8g8b' or turn_str == 'e1g1b':
            rock_lit = True
        elif turn_str == 'e1c1w' or turn_str == 'e8c8w' or turn_str == 'e1c1b' or turn_str == 'e8c8b':
            rock_long = True
        f = open('src/data.txt', 'w+')
        temp = turn_str[len(turn_str) - 1]
        f.write('%s %s' % (temp[:2], temp[2:4]))
        f.close()
        if eat == True:
            pass
        print(turn_str)
        print(eat,rock_long,rock_lit)

        return eat, rock_long, rock_lit, BOARD

